# dynamoDB

노 시퀄 데이터 
key-value 형식으로 저장됨
자유로운 형태로 테이블을 구성가능
aws에서 관리하며 aws에서 자동으로 분산처리를 한다

쿼리 + 조인을 어떻게 사용하냐

인덱스 사용

Local secondary index

인덱스는 키로 찾는것이 일반적인 방식임
하지만 경우에 따라 다른 조건이 필요함 -> 인덱싱이 안되있기 때문에 성능 이슈 발생

하지만 LSI는 인덱싱이 되어있지 않은 Key 값을 이용하여 조건을 만들때 새로운 테이블을 만들어 문제를 해결한다
예
A1 A2 A3 A4 A5 로 존재 한다고 했을때

A1 A2 에만 인덱싱이 되어있다면

A3를 조건으로 sort를 한다면 성능 이슈가 발생함

이때 LSI를 사용하게 되면 새로운 테이블을 만들어 문제를 해결하게함

A1 A3 A2
A1 A4 A2 A3
A1 A5 A2 A3 A4

와 같은 인덱스를 만듬

Global Secondary Index

LSI와 다르게 앞에 키 값이 고정 된것이 아니라 자유롭게 사용가능

RDB 의 veiw 와 비슷한 역할

차이점
로컬은 메인테이블의 값을 변경시 sync하게 변경
글로벌은 메인테이블의 값을 변경시 async하게 변경

로컬 10 기가 크기 제한
글로벌은 무제한

스케일링

크게 신경쓸 필요가 없음
throughput
파티션
스케일링은 분산구조
write capacity units 쓰는 속도 1k
read capacity units 읽는 속도  4k 
파티션의 역할과 사용처에 따라 적절하게 설정

파티션닝 math

파티션 하나당 사용할 수 있는 용량의 한계가 있음
계산에 따라 max에 맞게 사용

하나의 파티션이 용량이 커지면 자동으로 데이터를 쪼개서 관리함

그래서 중요한 것이 하나의 파티션에만 입출력이 많이 발생 하는 것이 아니라 
공평하게 입출력이 되는 것이 중요함

그렇지 않으면 hot key 현상이 일어나 throttling 이 일어남

data modeling

1:1 1:n 관계 => nosql??

1번 테이블 (1:1 관계)
Partition key Attribute
ssn=12345   Email:test@test.test license:12345678

license:12345678    Email:test@test.test ssn:12345

1:n 관계

만약에 특정 디바이스에서 특정 시간에 들어온 정보를 확인하고자 하면
Partiton key        SortKey         Attributes
Device=1            time=00:00:00   Temperature=30
Device=1            time=00:00:01   Temperature=30
Device=2            time=00:00:00   Temperature=30

라고 하면 일반적으로는 특정 디바이스의 값을 불러온 다음에 시간에 대한 정보를 다시 검색하는
방법을 사용한다
하지만 sort key를 사용하게 되면 디바이스만 검색하면 sortkey에 대한 결과는 범위로 쉽게 알 수 있다.

rich expressions

query/get/scan:ProductReviews.FiveStar[0]
다이나모 db가 더 일하게 한다. 왜냐하면 다이나모 db는 입출력 량에 따라 금액이 측정되기 때문에 입출력자체를 줄이는것이 중요하다.

Conditional expression
    attribute_not_exists 조건에 맞지 않으면
    attribute_exists 조건에 맞으면

best practice






## Indexing

## Scaling