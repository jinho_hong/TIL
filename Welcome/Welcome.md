1. Java version 은 1.8 이하로 설치해 주시기 바랍니다.
> react native 실행시 작동하지 않습니다.
만약 이미 최신의 Java version을 설치했다면 다음 링크를 참조해 주세요[자바 9 삭제법](https://gist.github.com/schnell18/bcb9833f725be22f6acd01f94b486392)
2. adb 설정
> Android Studio를 설치후
~/.bashrc 또는 ~/.zshrc (Oh My ZSH를 사용하는 경우) 끝에 다음 행을 추가하십시오.
```
export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```
> 터미널을 다시 시작하면 잘 할 수 있습니다. 👍
[링크](https://code.i-harness.com/ko/q/1debb05)
3. react native unauthorized for adb devices
> Close the emulator and run the command adb kill-server in the command prompt. If adb is not set in you path then you might run this command from directory where adb is present.
Click on Wipe Data option from avd Actions menu. 
![사진](https://i.stack.imgur.com/ioM5B.png)
4. Graphiql auth 
> Backend 설정이 끝나고 나면 [localhost:8000/graphiql](localhost:8000/graphiql) 로 접속할 경우 인증이 되지 않아 문제가 query를 작동 시킬수 없는 문제가 발생합니다. 이때 jwt를 임시로 발급하여 사용하실 수 있습니다.

1. [ModHeader](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj)  와 [insomnia](https://insomnia.rest/download/) ( 같은역할의 다른 Tool 도 상관없습니다.)를 다운 받습니다.
2. insomnia를 이용하여 http://localhost:8000/authentication에게 POST 방식으로 아래 Query를 Graphql 방식으로 보냅니다. 
![사진](https://i.imgur.com/XWO8XA2.png)
GraphQL
```
mutation GenerateHMMToken($phonenumber: String!, $password: String!) {
    generateHMMToken(Phonenumber: $phonenumber, Password: $password) {
        Token
    }
}
```

Query variable
```
{
	"phonenumber": "821000000000",
	"password": "Trll88112020"
}
```
3. Send 후 나오는 JWT token을 저장합니다.
4. JWT token 을 아래와 같은 방법으로 ModHeader에 적용시킵니다. 이때 Name 은 Authorization으로 합니다.
![사진](https://i.imgur.com/UlZn2Kx.png)

[ModHeader](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj) 

[insomnia](https://insomnia.rest/download/)

[JWT decoder](https://jwt.io/)

5. 아이디 비밀번호